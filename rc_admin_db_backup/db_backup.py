# -*- coding: utf-8 -*-

from datetime import date
import subprocess as sp
import configparser


class ConfigManager:
    def __init__(self):
        self.config = configparser.RawConfigParser()
        self.configFilePath = '/home/ec2-user/dev/rc_admin_daily_backup/hosts.cfg'
        self.file_path = None

    def open_file(self):
        self.file_path = None
        try:
            with open(self.configFilePath) as file_path:
                self.config.read_file(file_path)
        except:
            print("Can not open file", self.configFilePath)
            quit()

    def close_file(self):
        if self.file_path is not None:
            self.file_path.close()


class DbBackup(object):

    def __init__(self):
        self.cm = ConfigManager()
        self.connection_host = 'rc-admin'
        self.cm.open_file()

        self.user = self.cm.config.get(self.connection_host, 'db_user')
        self.password = self.cm.config.get(self.connection_host, 'db_password')
        self.host = self.cm.config.get(self.connection_host, 'host')
        self.database = self.cm.config.get(self.connection_host, 'database')

        self.cm.close_file()

    def initiate_backup(self):

        today = date.today()
        today = today.strftime('_%d_%m_%Y')

        # Ports are handled in ~/.ssh/config since we use OpenSSH
        command = "/usr/local/mysql/bin/mysqldump -h 172.15.77.199" + " -u " + self.user + " -p" + self.password + " " \
                  + self.database + " > ~/rc_admin_daily_backup/rc_admin_db_backup" + today + ".sql"

        ssh = sp.Popen(["ssh", "-i", "/home/ec2-user/dev/server_file/ps-server-2016.pem", self.host, command],
                               shell=False,
                               stdout=sp.PIPE,
                               stderr=sp.PIPE)
        result = ssh.stdout.readlines()

        error = ssh.stderr.readlines()
        print("Result : {}".format(result))
        print("Error : {}".format(error))


test = DbBackup()
test.initiate_backup()